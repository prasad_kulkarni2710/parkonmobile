package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;

/**
 * Created by root on 14/10/16.
 */
public class GetTotalCount extends AsyncTask<String, Void, String> {
    private String TAG = GetTotalCount.class.getSimpleName();
    String totalCount;
    String url;
    String jsonStr;
    TextView tvTotal;
    Context context;
    TextView tvTotalNo;

    public GetTotalCount( Context context, TextView tvTotal, TextView tvTotalNo ) {
        this.context = context;
        this.tvTotal = tvTotal;
        this.tvTotalNo = tvTotalNo;
    }

    @Override
    protected String doInBackground( String... urls ) {
        url = urls[0];
        HttpHandler sh = new HttpHandler();
        // Making a request to url and getting response
        try {
            jsonStr = sh.makeServiceCallPost(url);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        if (jsonStr != null) {
            try {
                JSONArray ar = new JSONArray(jsonStr);
                for (int i = 0; i < ar.length(); i++) {
                    totalCount = ar.getString(i);
                    Log.e("Result---------", ar.getString(i));
                }

            } catch (Exception e) {
                e.fillInStackTrace();
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");
            Toast.makeText(context.getApplicationContext(),
                    "Couldn't get data from server.Please try again later",
                    Toast.LENGTH_LONG).show();
        }
        return totalCount;
    }

    protected void onPostExecute( String jsonObject ) {
        tvTotal.setText("Total:");
        tvTotalNo.setText(jsonObject);
        Log.e("Total count is:", jsonObject);

    }
}