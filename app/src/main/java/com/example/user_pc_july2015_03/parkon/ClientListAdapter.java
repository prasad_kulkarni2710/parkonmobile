package com.example.user_pc_july2015_03.parkon;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 1/10/16.
 */
public class ClientListAdapter extends BaseAdapter  {
    private Context mContext;
    ArrayList<HashMap<String, String>> modelListItemses = new ArrayList<>();

    public ClientListAdapter(Context mContext, ArrayList<HashMap<String, String>> modelListItemses) {
        this.mContext = mContext;
        this.modelListItemses = modelListItemses;
    }


    @Override
    public int getCount() {
        return modelListItemses.size();
    }

    @Override
    public Object getItem(int position) {
        return modelListItemses.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
           }

    public static class ViewHolder{
        private TextView tvName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        LayoutInflater mInflater = (LayoutInflater)
                mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvName.setText(modelListItemses.get(position).get("name"));

        return convertView;
    }
}
