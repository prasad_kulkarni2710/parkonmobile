package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 14/10/16.
 */
public class GetClientList  extends AsyncTask<String, Void, String> {
    JSONArray jarray;
    private String TAG = ClientList.class.getSimpleName();
    ArrayList<HashMap<String, String>> clientList = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchList = new ArrayList<>();
    private ClientListAdapter clientListAdapter;
    Context context;
    String searchString;
    int textLength;
    String item;
    String url;
    ListView lvClient;
    EditText etSearchClient;
    ImageButton btClearClient;

    public GetClientList(Context context, ListView lvClient, EditText etSearchClient, ImageButton btClearClient) {
        this.context = context;
        this.lvClient = lvClient;
        this.etSearchClient = etSearchClient;
        this.btClearClient=btClearClient;
    }

    @Override
    protected String doInBackground(String... urls) {
        url=urls[0];
        HttpHandler sh = new HttpHandler();
        // Making a request to url and getting response
        String jsonStr = null;
        try {
            jsonStr = sh.makeServiceCallPost(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "Response from url: " + jsonStr);

        if(jsonStr!=null) {
            try {
                // Getting JSON Array node
                jarray = new JSONArray(jsonStr);
                // looping through All Clients
                for (int i = 0; i < jarray.length(); i++) {
                    JSONObject c = jarray.getJSONObject(i);

                    String id = c.getString("id");
                    String name = c.getString("name");

                    HashMap<String, String> clients = new HashMap<>();

                    clients.put("name", name);
                    clients.put("id", id);

                    // adding client to client list
                    clientList.add(clients);
                }
            } catch (JSONException e) {
                e.fillInStackTrace();
            }
        }else{
            Log.e(TAG, "Couldn't get json from server.");
            Toast.makeText(context.getApplicationContext(),
                    "Couldn't get data from server.Please try again later",
                    Toast.LENGTH_LONG).show();
        }

        return jsonStr;
    }

    @Override
    protected void onPostExecute(String result) {
        // search for client
        etSearchClient.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //get the text in the EditText
                searchString=etSearchClient.getText().toString();
                textLength=searchString.length();
                searchList.clear();

                for(int i=0;i<clientList.size();i++)
                {
                    item=clientList.get(i).get("name");
                    if(textLength<=item.length()){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(item.substring(0,textLength))){
                            searchList.add(clientList.get(i));
                            clientListAdapter =new ClientListAdapter(context,  searchList);
                            lvClient.setAdapter(clientListAdapter);
                        }
                    }
                }
                if( searchList.isEmpty()){
                    Toast.makeText(context.getApplicationContext(),
                            "No Items Matched", Toast.LENGTH_SHORT).show();
                }
                clientListAdapter.notifyDataSetChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        lvClient.scrollTo(0, lvClient.getHeight());

        setAdapter();
        lvClient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                lvClient.smoothScrollToPosition(lvClient.getCount());

                // Get client id and pass it to parking lot
                Intent intent = new Intent(context.getApplicationContext(),ParkingLot.class);
                HashMap<String, String> map = searchList.get(position);
                etSearchClient.setText(map.get("name"));
                etSearchClient.setSelection(etSearchClient.getText().length());
                intent.putExtra("id" ,map.get("id"));
                context.startActivity(intent);
            }
        });
        btClearClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearchClient.getText().clear();
            }
        });
    }

    private void setAdapter() {

        searchList=new ArrayList<HashMap<String, String>>(clientList);
        clientListAdapter = new ClientListAdapter(context , searchList);
        lvClient.setAdapter(clientListAdapter);
        lvClient.deferNotifyDataSetChanged();
        lvClient.invalidate();
    }
}
