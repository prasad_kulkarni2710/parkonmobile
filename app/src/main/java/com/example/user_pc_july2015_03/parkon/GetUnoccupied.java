package com.example.user_pc_july2015_03.parkon;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by root on 14/10/16.
 */
public class GetUnoccupied extends AsyncTask<String, Void,String> {
    String url;
    String jsonStr;
    String countStatus;
    private String TAG = GetTotalCount.class.getSimpleName();

    @Override
    protected String doInBackground(String... urls) {
        url = urls[0];
        HttpHandler sh = new HttpHandler();
        try {
            jsonStr = sh.makeServiceCallPost(url);
        }catch (Exception e){
            e.fillInStackTrace();
        }

        if(jsonStr!=null) {
            try {
                JSONArray array = new JSONArray(jsonStr);
                JSONArray first = array.getJSONArray(0);
                Log.e("Result---------", first.getString(1));
                countStatus = first.getString(1);

            } catch (NumberFormatException e) {
                e.fillInStackTrace();
            } catch (JSONException e) {
                e.fillInStackTrace();
            }catch (NullPointerException e){
                e.fillInStackTrace();
            }

        }else {
            Log.e(TAG, "Couldn't get json from server.");

        }
        return countStatus;
    }
    protected void onPostExecute(String jsonObject) {

    }

}