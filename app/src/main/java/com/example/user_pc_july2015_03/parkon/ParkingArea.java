package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by PC-NOV15-02 on 11/29/2016.
 */
public class ParkingArea extends AppCompatActivity {
    String strId;
    long lotId;
    TextView tvTotal;
    TextView tvAvail;
    TextView tvOccupied;
    TextView tvTotalNo;
    TextView tvOccupiedNo;
    TextView tvAvailNo;

    Spinner areaSpinner;
    String urlArea;
    Context context;
    ListView lvArea;
    TouchImageView touchImageView;
    Toolbar toolbar; ImageButton btClearArea;
    EditText etSearchArea;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parking_area);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        toolbar.setTitleTextColor(Color.parseColor("#1AD1FF"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.findViewById(R.id.toolbar_title1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back= new Intent(ParkingArea.this,ParkingLot.class);
                startActivity(back);
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        strId = extras.getString("id", null);
        lotId = Long.parseLong(strId);
        context = this;


        tvTotalNo=(TextView) findViewById(R.id.totalCountNo);

        tvTotal = (TextView) findViewById(R.id.totalCount);
        tvOccupied = (TextView) findViewById(R.id.occupied);
        tvOccupiedNo = (TextView) findViewById(R.id.occupiedNo);


        etSearchArea=(EditText)findViewById(R.id.searchTextArea);
        lvArea=(ListView)findViewById(R.id.parkingAreaList);
        btClearArea=(ImageButton)findViewById(R.id.clearArea);
        urlArea = "http://125.99.74.94:8443/parkingArea/list?parkingLotId=" + lotId;
        new GetParkingArea(context, lvArea,btClearArea,etSearchArea,tvTotal,tvTotalNo,tvOccupied,tvOccupiedNo).execute(urlArea);

    }

}
