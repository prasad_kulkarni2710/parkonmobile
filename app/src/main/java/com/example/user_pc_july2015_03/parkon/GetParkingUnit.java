package com.example.user_pc_july2015_03.parkon;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by root on 14/10/16.
 */
public class GetParkingUnit extends AsyncTask<String, Void, String> {
    private String TAG = GetParkingUnit.class.getSimpleName();

    String mapPath;
    int avail=0;
    Bitmap layout = null;
    Bitmap yellow = null;
    Bitmap red = null;
    Bitmap green = null;
    Bitmap gray = null;
    Bitmap low_battery = null;
    Bitmap resultBitmap;




    String aid;
    String url;
    int occupied;
    long areaId;
    TextView tvTotal;
    TextView tvAvail;
    TextView tvOccupied;
    String searchString;
    int textLength;
    String item;
    EditText etSearchArea;

    //Spinner areaSpinner;
    private ClientListAdapter areaAdapter;

    ArrayList<HashMap<String, String>> parkingAreaList = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchList = new ArrayList<>();

    ListView lvArea;
    ArrayAdapter<String> adapterSpinner;
    private ArrayList<ModelListItems> modelListItemses = new ArrayList<ModelListItems>();
    ArrayList<String> parkingArea = new ArrayList<>();
    String urlUnit;
    String urlUnoccupied;
    String urlTotalSlots;
    private ProgressDialog progressBar;
    Context context;
    TouchImageView touchImageView;
    TextView tvTotalNo;
    TextView tvOccupiedNo;
    TextView tvAvailNo;
    ImageButton btClearArea;

    public GetParkingUnit(Context context, TextView tvAvail, TextView tvAvailNo, TouchImageView touchImageView) {
        this.context = context;
        this.tvAvail = tvAvail;
        this.touchImageView=touchImageView;
        //this.ivMap = ivMap;
        this.tvAvailNo=tvAvailNo;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... urls) {
        // fetch car from server
        try {
            yellow=BitmapFactory.decodeStream((InputStream) new URL("http://125.99.74.94:8443/images/car_yellow.png").getContent());
            red=BitmapFactory.decodeStream((InputStream) new URL("http://125.99.74.94:8443/images/car_red.png").getContent());
            green=BitmapFactory.decodeStream((InputStream) new URL("http://125.99.74.94:8443/images/car_green.png").getContent());
            gray=BitmapFactory.decodeStream((InputStream) new URL("http://125.99.74.94:8443/images/car_gray.png").getContent());
            low_battery=BitmapFactory.decodeStream((InputStream) new URL("http://125.99.74.94:8443/images/low_battery.png").getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }

        url=urls[0];
        HttpHandler h=new HttpHandler();
        String jsonStr=h.makeServiceCallGet(url);
        Canvas canvas;
        if(jsonStr!=null) {
            try {
                JSONArray jarray = new   JSONArray(jsonStr);
                for (int i = 0; i < jarray.length(); i++) {
                    Log.e("Json data----:", jarray.getJSONObject(i).toString());

                    //areaName = jarray.getJSONObject(i).getJSONObject("parkingarea").getString("name");

                    // get map path
                    String server = "http://125.99.74.94:8443";
                    mapPath = server.concat(jarray.getJSONObject(i).getJSONObject("parkingarea").getString("map"));
                    layout = BitmapFactory.decodeStream((InputStream) new URL(mapPath).getContent());

                    resultBitmap = Bitmap.createBitmap(layout.getWidth(), layout.getHeight(), layout.getConfig());
                    canvas = new Canvas(resultBitmap);

                    //draw main layout
                    canvas.drawBitmap(layout, new Matrix(), null);

                    // draw car on main layout
                    for (int j = 0; i < jarray.length(); j++) {
                        Matrix scaleMatrix = new Matrix();
                        scaleMatrix.postTranslate(jarray.getJSONObject(j).getInt("x"), jarray.getJSONObject(j).getInt("y"));
                        if (jarray.getJSONObject(j).getInt("status") == 0) {
                            canvas.drawBitmap(yellow, scaleMatrix, new Paint());
                        }
                        if (jarray.getJSONObject(j).getInt("status") == 1) {
                            canvas.drawBitmap(red, scaleMatrix, new Paint());
                        }

                        if (jarray.getJSONObject(j).getInt("status") == 2) {
                            canvas.drawBitmap(green, scaleMatrix, new Paint());
                            avail++;
                        }
                        if (jarray.getJSONObject(j).getInt("status") == 3) {
                            canvas.drawBitmap(gray, scaleMatrix, new Paint());
                        }
                        if (jarray.getJSONObject(j).getInt("status") == 4) {
                            canvas.drawBitmap(gray, scaleMatrix, new Paint());
                        }
                        if (jarray.getJSONObject(j).getInt("status") == 5) {
                            canvas.drawBitmap(low_battery, scaleMatrix, new Paint());
                        }
                    }
                    if (isCancelled()) break;
                }
            } catch (JSONException e) {
                e.fillInStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Log.e(TAG, "Couldn't get json from server.");
            Toast.makeText(context.getApplicationContext(),
                    "Couldn't get data from server.Please try again later",
                    Toast.LENGTH_LONG).show();
        }
        return jsonStr;
    }
    protected void onPostExecute(String jsonObject) {
        //ivMap.setImageBitmap(resultBitmap);
        //ivMap.setOnTouchListener(new ZoomImage());
        touchImageView.setImageBitmap(resultBitmap);
        zoomImageView();
        tvAvail.setText("Available:");
        tvAvailNo.setText(String.valueOf(avail));
    }

    private void zoomImageView() {
        touchImageView.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {

            @Override
            public void onMove() {
                PointF point = touchImageView.getScrollPosition();
                RectF rect = touchImageView.getZoomedRect();
                float currentZoom = touchImageView.getCurrentZoom();
                boolean isZoomed = touchImageView.isZoomed();
            }
        });
    }






    public void executeAsynctask() {
        //Url to get parking unit map from JSON




        /*urlUnit = "http://125.99.74.94:8443/parkingUnit/list?parkingAreaId=" + areaId;
        new GetParkingUnit(context,tvAvail,tvAvailNo, touchImageView).execute(urlUnit);*/

        //Url to get total slots from JSON
        urlTotalSlots = "http://125.99.74.94:8443/parkingArea/fortotalslots?parkingAreaId=" + areaId;

        //Url to get occupied slots from JSON
        urlUnoccupied = "http://125.99.74.94:8443/getCountStatus?parkingAreaId=" + areaId;
        try {
            String total_count = new GetTotalCount(context, tvTotal,tvTotalNo).execute(urlTotalSlots).get();
            String unoccupied = new GetUnoccupied().execute(urlUnoccupied).get();
            int a = Integer.parseInt(total_count);
            int b = Integer.parseInt(unoccupied);
            occupied = a - b;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        tvOccupied.setText("Occupied:" + occupied);

        tvOccupied.setText("Occupied:");
        tvOccupiedNo.setText(String.valueOf(occupied));
    }
}
