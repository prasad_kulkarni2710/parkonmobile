package com.example.user_pc_july2015_03.parkon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/**
 * Created by root on 19/9/16.
 */
public class Splash extends AppCompatActivity {
    public static Object SPLASH_LOCK = new Object();

    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        final TextView tv= (TextView)findViewById(R.id.splash);
        String text1 = "<font COLOR=\'#FF1AD1FF\'><b>" + "ParkingArea" + "</b></font>"
                + "<font COLOR=\'#FFFFFF\'>" + "Üng" + "</font>";
        tv.setText(Html.fromHtml(text1));

        final Animation an= AnimationUtils.loadAnimation(getBaseContext(),R.anim.alpha);
        final Animation an1=AnimationUtils.loadAnimation(getBaseContext(),R.anim.abc_fade_out);
        tv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart( Animation animation ) {

            }

            @Override
            public void onAnimationEnd( Animation animation ) {
                tv.startAnimation(an1);

                Intent i= new Intent(getApplicationContext(),ClientList.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onAnimationRepeat( Animation animation ) {

            }
        });

    }

}
