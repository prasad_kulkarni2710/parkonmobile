package com.example.user_pc_july2015_03.parkon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog progressBar;
    EditText A_text1;
    EditText A_text2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(false);

        setContentView(R.layout.activity_main);

        /*ActionBar supportActionBar=getSupportActionBar();
        supportActionBar.setLogo(R.mipmap.logo);*/

        A_text1=(EditText) findViewById(R.id.editText);
        A_text2=(EditText) findViewById(R.id.editText2);
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {

                if (A_text1.getText().length()==0) {
                    A_text1.setError("Field is required");
                }else {
                    if (A_text2.getText().length() == 0) {
                        A_text2.setError("Field is required");
                    } else {
                        Toast.makeText(MainActivity.this, "Validation Successful", Toast.LENGTH_LONG).show();
                        progressBar = new ProgressDialog(v.getContext());
                        progressBar.setMessage("Loading...");
                        progressBar.setCancelable(false);
                        progressBar.show();

                        new Thread(new Runnable() {
                            public void run() {

                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                progressBar.dismiss();
                            }


                        }).start();
                        goTosecondactivity();
                    }
                }

            }

        });
    }
    private void goTosecondactivity() {

        Intent intent = new Intent(this, ClientList.class);

        startActivity(intent);

    }
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
