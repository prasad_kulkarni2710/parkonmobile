package com.example.user_pc_july2015_03.parkon;

/**
 * Created by root on 1/10/16.
 */
public class ModelListItems {

    private String id, name;

    public ModelListItems() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

