package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by PC-NOV15-02 on 9/17/2016.
 */
public class ParkingUnit extends AppCompatActivity {
    String strId;
    long areaId;
    TextView tvTotal;
    TextView tvAvail;
    TextView tvOccupied;
    TextView tvTotalNo;
    TextView tvOccupiedNo;
    TextView tvAvailNo;  String urlUnit;
    String urlTotalSlots;
    String urlUnoccupied;
    int occupied;

    //Spinner areaSpinner;
    String urlArea;
    Context context;
    //ListView lvArea;
    TouchImageView touchImageView;
    Toolbar toolbar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parking_unit);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        toolbar.setTitleTextColor(Color.parseColor("#1AD1FF"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.findViewById(R.id.toolbar_title1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back= new Intent(ParkingUnit.this,ParkingArea.class);
                startActivity(back);
                finish();
            }

        });


        Bundle extras = getIntent().getExtras();
        strId = extras.getString("id", null);
        areaId = Long.parseLong(strId);
        context = this;
        tvAvail = (TextView) findViewById(R.id.available);

      // ivMap = (ImageView) findViewById(R.id.map);
        touchImageView = (TouchImageView) findViewById(R.id.map);


        tvAvailNo = (TextView) findViewById(R.id.availableNo);
        tvTotalNo=(TextView) findViewById(R.id.totalCountNo);

        tvTotal = (TextView) findViewById(R.id.totalCount);
        tvOccupied = (TextView) findViewById(R.id.occupied);
        tvOccupiedNo = (TextView) findViewById(R.id.occupiedNo);
        //areaSpinner = (Spinner) findViewById(R.id.areaSpinner);
        //lvArea=(ListView)findViewById(R.id.parkingAreaList);
        touchImageView = (TouchImageView) findViewById(R.id.map);

        urlUnit = "http://125.99.74.94:8443/parkingUnit/list?parkingAreaId=" + areaId;
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            new GetParkingUnit(context,tvAvail,tvAvailNo, touchImageView).execute(urlUnit);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 1000); //execute in every 50000 ms
//Refreshing the map




        //Url to get total slots from JSON
        urlTotalSlots = "http://125.99.74.94:8443/parkingArea/fortotalslots?parkingAreaId=" +areaId;

        //Url to get occupied slots from JSON
        urlUnoccupied = "http://125.99.74.94:8443/getCountStatus?parkingAreaId=" +areaId;
        try {
            String total_count = new GetTotalCount(context, tvTotal,tvTotalNo).execute(urlTotalSlots).get();
            String unoccupied = new GetUnoccupied().execute(urlUnoccupied).get();
            int a = Integer.parseInt(total_count);
            int b = Integer.parseInt(unoccupied);
            occupied = a - b;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        tvOccupied.setText("Occupied:" + occupied);

        tvOccupied.setText("Occupied:");
        tvOccupiedNo.setText(String.valueOf(occupied));
    }

   /* @Override
    public void onBackPressed() {
        GetParkingUnit d=new GetParkingUnit(context,tvAvail,tvAvailNo, touchImageView);
        d.cancel(true);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GetParkingUnit d=new GetParkingUnit(context,tvAvail,tvAvailNo, touchImageView);
        d.cancel(true);
    }
}