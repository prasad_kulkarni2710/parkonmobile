package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;

public  class ParkingLot extends AppCompatActivity {
    String strId;
    long clientId;
    String urlParkingLot;
   // ExpandableListView lvLot;
    ListView lvLot;
    Context context;
    EditText etSearchLot;
    Toolbar toolbar;
    ImageButton btClearLot;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parkinglot);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        btClearLot=(ImageButton)findViewById(R.id.clearLot);

        toolbar.setTitleTextColor(Color.parseColor("#1AD1FF"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.findViewById(R.id.toolbar_title1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back= new Intent(ParkingLot.this,ClientList.class);
                startActivity(back);
                finish();
            }
        });

        context=this;
        Bundle extras = getIntent().getExtras();
        strId =extras.getString("id");
        clientId=Long.parseLong(strId);
        // URL to get parking lot from JSON
        urlParkingLot = "http://125.99.74.94:8443/parkingLot/list?clientId="+clientId;
        etSearchLot=(EditText)findViewById(R.id.searchText1);
        lvLot = (ListView) findViewById(R.id.parkingLotList);
        new GetParkingLot(context,lvLot,etSearchLot,btClearLot).execute(urlParkingLot);
    }
}