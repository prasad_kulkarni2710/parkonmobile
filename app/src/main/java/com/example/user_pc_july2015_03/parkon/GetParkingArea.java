package com.example.user_pc_july2015_03.parkon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;



/**
 * Created by root on 14/10/16.
 */
public class GetParkingArea extends AsyncTask<String,Void,String> {

    private String TAG = ParkingUnit.class.getSimpleName();

    String aid;
    String url;
    int occupied;
    long areaId;
    TextView tvTotal;
    TextView tvAvail;
    TextView tvOccupied;
    String searchString;
    int textLength;
    String item;
    EditText etSearchArea;

    //Spinner areaSpinner;
    private ClientListAdapter areaAdapter;

    ArrayList<HashMap<String, String>> parkingAreaList = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchList = new ArrayList<>();

    ListView lvArea;
    ArrayAdapter<String> adapterSpinner;
    private ArrayList<ModelListItems> modelListItemses = new ArrayList<ModelListItems>();
    ArrayList<String> parkingArea = new ArrayList<>();
    String urlUnit;
    String urlUnoccupied;
    String urlTotalSlots;
    private ProgressDialog progressBar;
    Context context;
    TouchImageView touchImageView;
    TextView tvTotalNo;
    TextView tvOccupiedNo;
    TextView tvAvailNo;
    ImageButton btClearArea;
    public GetParkingArea(Context context, ListView lvArea, TextView tvOccupied, TouchImageView touchImageView, TextView tvAvail, TextView tvTotal, TextView tvTotalNo, TextView tvOccupiedNo, TextView tvAvailNo) {
        this.context = context;
        this.lvArea=lvArea;
        this.tvOccupied = tvOccupied;
        this.touchImageView=touchImageView;
        this.tvAvail = tvAvail;
        this.tvTotal = tvTotal;
        this.tvTotalNo=tvTotalNo;
        this.tvOccupiedNo=tvOccupiedNo;
        this.tvAvailNo=tvAvailNo;

    }

    public GetParkingArea(Context context, ListView lvArea, ImageButton btClearArea, EditText etSearchArea, TextView tvTotal, TextView tvTotalNo, TextView tvOccupied, TextView tvOccupiedNo) {
        this.context = context;
        this.lvArea=lvArea;
        this.btClearArea= btClearArea;
        this.etSearchArea=etSearchArea;
        this.tvTotal = tvTotal;
        this.tvTotalNo=tvTotalNo;

        this.tvOccupied=tvOccupied;
        this.tvOccupiedNo=tvOccupiedNo;


    }

    @Override
    protected String doInBackground(String... urls) {
        url = urls[0];
        HttpHandler sh = new HttpHandler();
        String jsonStr = null;
        try {
            jsonStr = sh.makeServiceCallPost(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "Response from url: " + jsonStr);
        if (jsonStr != null) {
            try {
                JSONArray jarray = new JSONArray(jsonStr);
                for (int i = 0; i < jarray.length() && !isCancelled(); i++) {
                    JSONObject c = jarray.getJSONObject(i);
                    String id = c.getString("id");
                    String name=c.getString("name");

                    HashMap<String, String> area = new HashMap<>();
                    area.put("name", name);
                    area.put("id", id);

                    // adding contact to contact list
                    parkingAreaList.add(area);


                   /* ModelListItems modelListItems = new ModelListItems();
                    modelListItems.setId(id);
                    modelListItemses.add(modelListItems);
                    // adding contact to contact list
                    parkingArea.add(c.getString("name"));*/
                }
            } catch (final JSONException e) {
                e.fillInStackTrace();
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");
            Toast.makeText(context.getApplicationContext(),
                    "Couldn't get data from server.Please try again later",
                    Toast.LENGTH_LONG).show();
        }
        return null;
    }

    protected void onPostExecute(String jsonobject) {
       /* adapterSpinner=new ArrayAdapter<String>(context,R.layout.list_item,R.id.name,parkingArea)
        {
            @SuppressLint("ResourceAsColor")
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                return view;
            }

        };
        areaSpinner.setAdapter(adapterSpinner);
        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                aid = modelListItemses.get(position).getId();
                areaId = Long.parseLong(aid);

                executeAsynctask();

                progressBar = new ProgressDialog(selectedItemView.getContext());
                progressBar.setMessage("Loading...");
                progressBar.setCancelable(false);
                progressBar.show();

                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        progressBar.dismiss();
                    }

                }).start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });*/


        etSearchArea.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //get the text in the EditText
                searchString=etSearchArea.getText().toString();
                textLength=searchString.length();
                searchList.clear();

                for(int i=0;i<parkingAreaList.size();i++)
                {
                    item=parkingAreaList.get(i).get("name");
                    if(textLength<=item.length()){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(item.substring(0,textLength))){
                            searchList.add(parkingAreaList.get(i));
                            areaAdapter =new ClientListAdapter(context,  searchList);
                            lvArea.setAdapter( areaAdapter);
                        }
                    }
                }
                if( searchList.isEmpty()){
                    Toast.makeText(context.getApplicationContext(),
                            "No Items Matched", Toast.LENGTH_SHORT).show();
                }
                areaAdapter.notifyDataSetChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });


        lvArea.scrollTo(0, lvArea.getHeight());
        setAdapter();


        lvArea.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                lvArea.smoothScrollToPosition(lvArea.getCount());

              //  executeAsynctask();

                // Get parking lots and pass lot id to parking area
                Intent intent = new Intent(context.getApplicationContext(), ParkingUnit.class);
                HashMap<String, String> map = searchList.get(position);
                etSearchArea.setText(map.get("name"));
                etSearchArea.setSelection(etSearchArea.getText().length());
                intent.putExtra("id", map.get("id"));
                context.startActivity(intent);


            }

        });
        btClearArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearchArea.getText().clear();
            }
        });
    }

    private void setAdapter() {

        // listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

        // setting list adapter
        //lvLot.setAdapter(listAdapter);


        searchList = new ArrayList<HashMap<String, String>>(parkingAreaList);
        areaAdapter = new ClientListAdapter(context, searchList);
        lvArea.setAdapter(areaAdapter);


        //listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

        // setting list adapter
        // expListView.setAdapter(listAdapter);


        //lvLot.setAdapter(lotAdapter);
        lvArea.deferNotifyDataSetChanged();
        lvArea.invalidate();
    }




    /*public void executeAsynctask() {
        //Url to get parking unit map from JSON




        *//*urlUnit = "http://125.99.74.94:8443/parkingUnit/list?parkingAreaId=" + areaId;
        new GetParkingUnit(context,tvAvail,tvAvailNo, touchImageView).execute(urlUnit);*//*

        //Url to get total slots from JSON
        urlTotalSlots = "http://125.99.74.94:8443/parkingArea/fortotalslots?parkingAreaId=" + areaId;

        //Url to get occupied slots from JSON
        urlUnoccupied = "http://125.99.74.94:8443/getCountStatus?parkingAreaId=" + areaId;
        try {
            String total_count = new GetTotalCount(context, tvTotal,tvTotalNo).execute(urlTotalSlots).get();
            String unoccupied = new GetUnoccupied().execute(urlUnoccupied).get();
            int a = Integer.parseInt(total_count);
            int b = Integer.parseInt(unoccupied);
            occupied = a - b;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        tvOccupied.setText("Occupied:" + occupied);

        tvOccupied.setText("Occupied:");
        tvOccupiedNo.setText(String.valueOf(occupied));
    }*/
}
