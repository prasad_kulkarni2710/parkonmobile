package com.example.user_pc_july2015_03.parkon;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 14/10/16.
 */
public class GetParkingLot  extends AsyncTask<String, Void, String> {


    ExpandableListAdapter listAdapter;
   // ExpandableListView expListView;


    private String TAG = ClientList.class.getSimpleName();
    ArrayList<HashMap<String, String>> parkingLot = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchList = new ArrayList<>();
    List<String> listDataHeader = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
    List<String> top= new ArrayList<String>();
    private ClientListAdapter lotAdapter;
    String url;
    private ListView lvLot;
    String searchString;
    int textLength;
    String item;
    EditText etSearchLot;
    Context context;
    ImageButton  btClearLot;

    public GetParkingLot(Context context, ListView lvLot, EditText etSearchLot, ImageButton btClearLot) {
        this.context = context;
        this.lvLot = lvLot;
        this.etSearchLot = etSearchLot;
        this.btClearLot=btClearLot;
    }

    @Override
    protected String doInBackground(String... urls) {
        url = urls[0];
        HttpHandler sh = new HttpHandler();
        // Making a request to url and getting response
        String jsonStr = null;
        try {
            jsonStr = sh.makeServiceCallPost(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "Response from url: " + jsonStr);

        if (jsonStr != null) {
            try {
                // Getting JSON Array node
                JSONArray jarray = new JSONArray(jsonStr);
                // looping through All Contacts
                for (int i = 0; i < jarray.length(); i++) {
                    JSONObject c = jarray.getJSONObject(i);

                    String id = c.getString("id");
                    String name = c.getString("name");

                   /* listDataHeader.add(name);

                    top.add(name);

                    listDataChild.put(listDataHeader.get(i), top);*/

                    HashMap<String, String> lot = new HashMap<>();
                    lot.put("name", name);
                    lot.put("id", id);

                    // adding contact to contact list
                    parkingLot.add(lot);
                }


            } catch (final JSONException e) {
                e.fillInStackTrace();
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");

            Toast.makeText(context.getApplicationContext(),
                    "Couldn't get data from server.Please try again later",
                    Toast.LENGTH_LONG).show();
        }

        return jsonStr;
    }

    @Override
    protected void onPostExecute(String result) {

        etSearchLot.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //get the text in the EditText
                searchString=etSearchLot.getText().toString();
                textLength=searchString.length();
                searchList.clear();

                for(int i=0;i<parkingLot.size();i++)
                {
                    item=parkingLot.get(i).get("name");
                    if(textLength<=item.length()){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(item.substring(0,textLength))){
                            searchList.add(parkingLot.get(i));
                            lotAdapter =new ClientListAdapter(context,  searchList);
                            lvLot.setAdapter( lotAdapter);
                        }
                    }
                }
                if( searchList.isEmpty()){
                    Toast.makeText(context.getApplicationContext(),
                            "No Items Matched", Toast.LENGTH_SHORT).show();
                }
                lotAdapter.notifyDataSetChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        lvLot.scrollTo(0, lvLot.getHeight());
        setAdapter();


        lvLot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                lvLot.smoothScrollToPosition(lvLot.getCount());

                // Get parking lots and pass lot id to parking area
                Intent intent = new Intent(context.getApplicationContext(), ParkingArea.class);
                HashMap<String, String> map = searchList.get(position);
                etSearchLot.setText(map.get("name"));
                etSearchLot.setSelection(etSearchLot.getText().length());
                intent.putExtra("id", map.get("id"));
                context.startActivity(intent);
            }

        });
        btClearLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearchLot.getText().clear();
            }
        });
    }

    private void setAdapter() {

       // listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

        // setting list adapter
        //lvLot.setAdapter(listAdapter);


        searchList = new ArrayList<HashMap<String, String>>(parkingLot);
        lotAdapter = new ClientListAdapter(context, searchList);
        lvLot.setAdapter(lotAdapter);


        //listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

        // setting list adapter
       // expListView.setAdapter(listAdapter);


        //lvLot.setAdapter(lotAdapter);
        lvLot.deferNotifyDataSetChanged();
        lvLot.invalidate();
    }
}
